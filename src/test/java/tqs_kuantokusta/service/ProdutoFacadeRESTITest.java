/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tqs_kuantokusta.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import javax.ejb.embeddable.EJBContainer;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import tqs_kuantokusta.Produto;

/**
 *
 * @author hrcpi
 */
public class ProdutoFacadeRESTITest {

    private String end;
    private URL url;
    private HttpURLConnection con;

    @Before
    public void setUp() throws MalformedURLException, IOException {
        end = "http://deti-tqs-03.ua.pt:8080/TQS-KuantoKusta/webresources/produto/";
        url = new URL(end);
        con = (HttpURLConnection) url.openConnection();
    }

    @Test
    public void checkConnection() throws IOException {
        con.setRequestMethod("GET");
        con.setRequestProperty("Accept", "application/json");
        Assert.assertEquals(200, con.getResponseCode());
    }

    @Test
    public void testFind_Integer() throws Exception {
        url = new URL(end + "/find/1");
        con.setRequestMethod("GET");
        con.setRequestProperty("Accept", "application/json");
        Assert.assertEquals(200, con.getResponseCode());

        BufferedReader br = new BufferedReader(new InputStreamReader((con.getInputStream())));
        String output = br.readLine();
        Assert.assertNotEquals("{\"descricao\":\"Banana da Madeira\",\"id\":1,\"idCategoria\":{\"id\":1,\"nome\":\"Fruta\"},\"idMarca\":{\"id\":1,\"nome\":\"Continente\"},\"img\":\"http://frutada5a.pt/image/cache/catalog/Destaques/bananamadeira500500d-500x500.png\",\"nome\":\"Banana\"}", output);
    }

    @Test
    public void testFindByMarca_Integer() throws Exception {
        url = new URL(end + "/bymarca/1");
        con.setRequestMethod("GET");
        con.setRequestProperty("Accept", "application/json");
        Assert.assertEquals(200, con.getResponseCode());

        BufferedReader br = new BufferedReader(new InputStreamReader((con.getInputStream())));
        String output = br.readLine();
        Assert.assertTrue(output.contains("{\"descricao\":\"Banana da Madeira\",\"id\":1,\"idCategoria\":{\"id\":1,\"nome\":\"Fruta\"},\"idMarca\":{\"id\":1,\"nome\":\"Continente\"},\"img\":\"http://frutada5a.pt/image/cache/catalog/Destaques/bananamadeira500500d-500x500.png\",\"nome\":\"Banana\"}"));
    }
    
    @Test
    public void testFindByCategoria_Integer() throws Exception {
        url = new URL(end + "/bycategoria/1");
        con.setRequestMethod("GET");
        con.setRequestProperty("Accept", "application/json");
        Assert.assertEquals(200, con.getResponseCode());

        BufferedReader br = new BufferedReader(new InputStreamReader((con.getInputStream())));
        String output = br.readLine();
        Assert.assertTrue(output.contains("{\"descricao\":\"Banana da Madeira\",\"id\":1,\"idCategoria\":{\"id\":1,\"nome\":\"Fruta\"},\"idMarca\":{\"id\":1,\"nome\":\"Continente\"},\"img\":\"http://frutada5a.pt/image/cache/catalog/Destaques/bananamadeira500500d-500x500.png\",\"nome\":\"Banana\"}"));
    }

    @Test
    public void testFindAll() throws Exception {
        url = new URL(end + "/find/");
        con.setRequestMethod("GET");
        con.setRequestProperty("Accept", "application/json");
        Assert.assertEquals(200, con.getResponseCode());
    }

}
