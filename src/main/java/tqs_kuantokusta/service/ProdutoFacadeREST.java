/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tqs_kuantokusta.service;

import tqs_kuantokusta.Produto;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import tqs_kuantokusta.Categoria;
import tqs_kuantokusta.Marca;

/**
 *
 * @author hrcpi
 */
@Stateless
@Path("produto")
public class ProdutoFacadeREST extends AbstractFacade<Produto> {

    @EJB
    CategoriaFacadeREST facade_categoria = new CategoriaFacadeREST();

    @EJB
    MarcaFacadeREST facade_marca = new MarcaFacadeREST();

    @PersistenceContext(unitName = "com.mycompany_mavenproject1_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    public ProdutoFacadeREST() {
        super(Produto.class);
    }

    @POST
    @Override
    @Consumes(MediaType.APPLICATION_JSON)
    public void create(Produto entity) {
        super.create(entity);
    }

    @PUT
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public void edit(@PathParam("id") Integer id, Produto entity) {
        super.edit(entity);
    }

    @GET
    @Path("/bycategoria/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public List<Produto> findByCategoria(@PathParam("id") Integer id_cat) {
        Categoria cat = facade_categoria.find(id_cat);
        List<Produto> prod_list;
        return prod_list = em.createNamedQuery("Produto.findByCategoria")
                .setParameter("idCategoria", cat)
                .getResultList();

    }

    @GET
    @Path("/bymarca/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Produto> findByMarca(@PathParam("id") Integer id_marca) {
        Marca marca = facade_marca.find(id_marca);
        List<Produto> prod_list;
        return prod_list = em.createNamedQuery("Produto.findByMarca")
                .setParameter("idMarca", marca)
                .getResultList();
    }

    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") Integer id) {
        super.remove(super.find(id));
    }

    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Produto find(@PathParam("id") Integer id) {
        return super.find(id);
    }

    @GET
    @Override
    @Produces(MediaType.APPLICATION_JSON)
    public List<Produto> findAll() {
        return super.findAll();
    }

    @GET
    @Path("{from}/{to}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Produto> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return super.findRange(new int[]{from, to});
    }

    @GET
    @Path("count")
    @Produces(MediaType.TEXT_PLAIN)
    public String countREST() {
        return String.valueOf(super.count());
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

}
